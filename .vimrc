:set autoindent
:set number
:set cindent

execute pathogen#infect()

autocmd vimenter * NERDTree
